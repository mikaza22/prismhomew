﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;

namespace PrismHomeW.ViewModels
{
    public class PrismHomeWPageViewModel : BindableBase, INavigationAware 
    {

        public Command RunCommand { get; set; }

        INavigationService _navigationService;

        private string _time = string.Empty;
        public string Time
        {
            get { return _time; }
            set {SetProperty(ref _time, value);}
        }

        string _date = string.Empty;
        public string Date
        {
            get { return _date; }
            set { SetProperty(ref _date, value); }
        }

        string _location = string.Empty;
        public string Location
        {
            get { return _location; }
            set { SetProperty(ref _location, value); }
        }

        string _weather = string.Empty;
        public string Weather
        {
            get { return _weather; }
            set { SetProperty(ref _weather, value); }
        }

        public PrismHomeWPageViewModel(INavigationService navigationService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}:ctor");
            _navigationService = navigationService;
            RunCommand = new Command(onRun);
            Time = "Hour";
            Date = "Date";
            Location = "Location";
            Weather = "Weather";
        }

        private void onRun(object param)
        {
            switch (param)
            {
                case "0":
                    if (Time == "Hour")
                    {
                        Time = DateTime.Now.ToString("H:mm");
                    }
                    else
                    {
                        Time = "Hour";
                    }
                    break;
                case "1":
                    if (Date == "Date")
                    {
                        Date = DateTime.Now.ToString("MMMM dd, yyyy");
                    }
                    else
                    {
                        Date = "Date";
                    }
                    break;
                case "2":
                    if (Location == "Location")
                    {
                        Location = System.Globalization.RegionInfo.CurrentRegion.EnglishName;
                    }
                    else
                    {
                        Location= "Location";
                    }
                    break;
                case "3":
                    if (Weather == "Weather")
                    {
                        Weather = "Sunny, it's California";
                    }
                    else
                    {
                        Weather = "Weather";
                    }
                    break;
            }
        }

        #region INavigationAware implementation

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }

        #endregion INavigationAware implementation

    }
}