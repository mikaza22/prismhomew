﻿using System.Diagnostics;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using PrismHomeW.Views;
using PrismHomeW.ViewModels;
using Xamarin.Forms;

namespace PrismHomeW
{
    public partial class App : PrismApplication
    {
     
        public App(IPlatformInitializer initializer = null) : base(initializer) {}

        protected override void OnInitialized()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();
            NavigationService.NavigateAsync(nameof(PrismHomeWPage));
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");
            containerRegistry.RegisterForNavigation<PrismHomeWPage, PrismHomeWPageViewModel>();
        }

        protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            // Handle when your app resumes
        }
    }
}
